use actix_web::{
	web::{block, Data, Path, ReqData, Json, Query},
	Error, HttpResponse,
};
use serde_json::json;
use uuid::Uuid;

use crate::{auth::models::Claims, DBPool, common::models::Metadata};
use crate::member::services::remove_totally_member;
use super::{services::{find_one_member_by_id, update_member, find_all_members, count_all_members}, models::{UpdateMember, RetrieveMembersQueryParams}};

pub async fn retrieve_member_me(
	claims: ReqData<Claims>,
	db: Data<DBPool>,
) -> Result<HttpResponse, Error> {
	let conn = db.get().unwrap();
	let member_id: Uuid = Uuid::parse_str(&claims.sub.clone()).unwrap();

	let member_found = block(move || find_one_member_by_id(member_id, &conn))
		.await
		.unwrap();

	match member_found {
		Ok(member) => Ok(HttpResponse::Ok().json(member)),
		Err(err) => return Ok(HttpResponse::NotFound().json(json!({ "message": format!("Member not found: {}", err.to_string()) }))),
	}
}

pub async fn delete_member_me(
	claims: ReqData<Claims>,
	db: Data<DBPool>,
) -> Result<HttpResponse, Error> {
	let member_id: Uuid = Uuid::parse_str(&claims.sub.clone()).unwrap();
	return remove_totally_member(member_id, db)
		.await;
}

pub async fn delete_member_by_id(
	path_member_id: Path<Uuid>,
	db: Data<DBPool>,
) -> Result<HttpResponse, Error> {
	let member_id: Uuid = path_member_id.into_inner();
	return remove_totally_member(member_id, db)
		.await;
}

pub async fn update_member_by_id(
	path_member_id: Path<Uuid>,
	member: Json<UpdateMember>,
	db: Data<DBPool>,
) -> Result<HttpResponse, Error> {
	let member_id: Uuid = path_member_id.into_inner();
	let conn = db.get().unwrap();
	let update_member = block(move || update_member(member_id, member.into_inner(), &conn))
		.await
		.unwrap();

	match update_member {
		Ok(member) => Ok(HttpResponse::Ok().json(member)),
		Err(_) => return Ok(HttpResponse::NotFound().finish()),
	}
}

pub async fn retrieve_members(
	query_params: Query<RetrieveMembersQueryParams>,
	db: Data<DBPool>,
) -> Result<HttpResponse, Error> {
	let query_members = query_params.clone();
	let conn_members = db.get().unwrap();
	let members = block(move || find_all_members(
		query_members.search.clone(),
		query_members.limit,
		query_members.offset,
		&conn_members
	))
		.await
		.unwrap();

	if members.is_err() {
		return Ok(HttpResponse::InternalServerError().json(json!({ "message": "Internal server error" })));
	}

	let query_count = query_params.clone();
	let conn_count = db.get().unwrap();
	let count = block(move || count_all_members(
		query_count.search.clone(),
		&conn_count
	))
		.await
		.unwrap();

	if count.is_err() {
		return Ok(HttpResponse::InternalServerError().json(json!({ "message": "Internal server error" })));
	}

	let metadata = Metadata {
		count: count.unwrap(),
		limit: query_params.limit,
		offset: query_params.offset,
		search: query_params.search.clone(),
	};

	return Ok(HttpResponse::Ok().json(json!({ "members": members.unwrap(), "metadata": metadata })));
}