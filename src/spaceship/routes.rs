use actix_web::{web::{ReqData, Data, block}, HttpResponse, Error};
use serde_json::json;
use uuid::Uuid;

use crate::{auth::models::Claims, DBPool};

use super::services::find_spaceship_list_by_member;

pub async fn retrieve_spaceships_by_member(claims: ReqData<Claims>, db: Data<DBPool>) -> Result<HttpResponse, Error> {
	let conn = db.get().unwrap();

	let member_id: Uuid = Uuid::parse_str(&claims.sub.clone()).unwrap();

	let spaceships_found = block(move || find_spaceship_list_by_member(&member_id, &conn))
		.await
		.unwrap();

	match spaceships_found {
		Ok(spaceships) => Ok(HttpResponse::Ok().json(spaceships)),
		Err(err) => {
			return Ok(HttpResponse::NotFound().json(json!({
				"message": format!("No spaceship owned found: {}", err.to_string()),
			})));
		},
	}	
}