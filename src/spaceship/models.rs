use serde::{Deserialize, Serialize};
use diesel::Insertable;

use crate::schema::spaceship::spaceship;

#[derive(Insertable, Debug, Deserialize, Serialize)]
#[table_name = "spaceship"]
pub struct NewSpaceship {
	pub spaceship_model_id: uuid::Uuid,
	pub member_id: uuid::Uuid,
	pub health: i32,
	pub is_main_ship: bool,
}

#[derive(Debug, Queryable, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Spaceship {
	pub spaceship_id: uuid::Uuid,
	pub spaceship_model_id: uuid::Uuid,
	pub name: Option<String>,
	pub member_id: uuid::Uuid,
	pub health: i32,
	pub is_main_ship: bool,
	pub created_at: Option<chrono::NaiveDateTime>,
	pub updated_at: Option<chrono::NaiveDateTime>,
}