use crate::diesel::ExpressionMethods;
use crate::shield_model::models::NewShieldModel;
use crate::DBPooledConnection;
use crate::{diesel::RunQueryDsl, schema::spaceship::shield_model};
use actix_web::{web::Json, Result};
use diesel::{sql_query, sql_types, QueryDsl};
use uuid::Uuid;

use super::models::ShieldModel;

pub fn insert_shield_model(
    shield_model: Json<NewShieldModel>,
    conn: &DBPooledConnection,
) -> Result<ShieldModel, diesel::result::Error> {
    return diesel::insert_into(shield_model::table)
        .values(shield_model.into_inner())
        .get_result(conn);
}

pub fn find_one_shield_model_by_name(
    name: String,
    conn: &DBPooledConnection,
) -> Result<ShieldModel, diesel::result::Error> {
    return shield_model::table
        .filter(shield_model::name.eq(name))
        .first::<ShieldModel>(conn);
}

pub fn find_one_shield_model_by_id(
    shield_model_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<ShieldModel, diesel::result::Error> {
    return shield_model::table
        .filter(shield_model::shield_model_id.eq(shield_model_id))
        .first::<ShieldModel>(conn);
}

pub fn find_one_shield_model(
    shield_model_id: Uuid,
    conn: &DBPooledConnection,
) -> Result<ShieldModel, diesel::result::Error> {
    return shield_model::table
        .filter(shield_model::shield_model_id.eq(shield_model_id))
        .first::<ShieldModel>(conn);
}

pub fn find_one_shield_model_by_shield_id(
    shield_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<ShieldModel, diesel::result::Error> {
    return sql_query(
        "SELECT * FROM spaceship.shield_model WHERE shield_model_id = (SELECT shield_model_id FROM spaceship.shield WHERE shield_id = $1)",
    )
    .bind::<sql_types::Uuid, _>(shield_id)
    .get_result::<ShieldModel>(conn);
}

pub fn find_all_shield_models(
    conn: &DBPooledConnection,
) -> Result<Vec<ShieldModel>, diesel::result::Error> {
    return shield_model::table.load::<ShieldModel>(conn);
}

pub fn find_shield_models_by_member(
    member_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<Vec<ShieldModel>, diesel::result::Error> {
    let string_query = include_str!("queries/find_shield_models_by_member.sql");

    return sql_query(string_query)
        .bind::<sql_types::Uuid, _>(member_id)
        .get_results(conn);
}

pub fn update_shield_model(
    shield_model_id: Uuid,
    shield_model: NewShieldModel,
    conn: &DBPooledConnection,
) -> Result<ShieldModel, diesel::result::Error> {
    return diesel::update(shield_model::table)
        .filter(shield_model::shield_model_id.eq(shield_model_id))
        .set(shield_model)
        .get_result(conn);
}

pub fn delete_shield_model(
	shield_model_id: &Uuid,
	conn: &DBPooledConnection,
) -> Result<usize, diesel::result::Error> {
	let result = diesel::delete(shield_model::table)
		.filter(shield_model::shield_model_id.eq(shield_model_id))
		.execute(conn);
	if Ok(0) == result {
		return Err(diesel::result::Error::NotFound);
	}
	return result;
}