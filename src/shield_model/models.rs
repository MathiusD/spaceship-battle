use crate::schema::spaceship::shield_model;
use diesel_derive_enum::DbEnum;
use serde::{Serialize, Deserialize};
use diesel::{Insertable, Queryable};
use uuid::Uuid;


#[derive(Debug, Serialize, Clone, DbEnum, Deserialize)]
#[DbValueStyle = "SCREAMING_SNAKE_CASE"]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]

pub enum ShieldType {
	Basic,
	Dispersion,
}

#[derive(Debug, Queryable, QueryableByName, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
#[table_name = "shield_model"]
pub struct ShieldModel {
	pub shield_model_id: Uuid,
	pub name: String,
	pub shield_type: ShieldType,
	pub shield_type_protection: Option<i32>,
	pub max_health: i32,
	pub reload_time: i32,
	pub max_power: i32,
	pub size: i32,
	pub price: i32,
}

#[derive(Insertable, Debug, Deserialize, Serialize, AsChangeset)]
#[table_name = "shield_model"]
#[serde(rename_all(deserialize = "camelCase"))]
pub struct NewShieldModel {
	pub name: String,
	pub shield_type: ShieldType,
	pub shield_type_protection: Option<i32>,
	pub max_health: i32,
	pub reload_time: i32,
	pub max_power: i32,
	pub size: i32,
	pub price: i32,
}