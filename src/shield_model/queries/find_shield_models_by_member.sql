SELECT
    "shield_model".shield_model_id,
    "shield_model".name,
    "shield_model".shield_type,
    "shield_model".shield_type_protection,
    "shield_model".reload_time,
    "shield_model".max_power,
    "shield_model".max_health,
    "shield_model".size,
    "shield_model".price
FROM "spaceship".shield
JOIN "spaceship".shield_model ON "shield".shield_model_id = "shield_model".shield_model_id
WHERE "shield".member_id = $1