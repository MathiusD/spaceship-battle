SELECT
	"spaceship_model".spaceship_model_id,
	"spaceship_model".name,
	"spaceship_model".max_health,
	"spaceship_model".spaceship_type,
	"spaceship_model".tank_capacity,
	"spaceship_model".max_power,
	"spaceship_model".consumption_for_power,
	"spaceship_model".energy_repartition_delay,
	"spaceship_model".price,
	JSONB_AGG(
		DISTINCT JSONB_BUILD_OBJECT(
			'module_zone_id', "module_zone".module_zone_id,
			'shielding', "module_zone".shielding
		)
	) AS module_zones,
	JSONB_AGG(
		DISTINCT JSONB_BUILD_OBJECT(
			'module_slot_id', "module_slot".module_slot_id,
			'module_zone_id', "module_slot".module_zone_id,
			'module_type', "module_slot".module_type,
			'size', "module_slot".size
		)
	) AS module_slots
--!find_by_id! FROM "spaceship".spaceship_model
--!find_by_member! FROM "spaceship".spaceship
--!find_by_member! JOIN "spaceship".spaceship_model ON "spaceship".spaceship_model_id = "spaceship_model".spaceship_model_id
--!find_all! FROM "spaceship".spaceship_model
JOIN "spaceship".module_zone ON "spaceship_model".spaceship_model_id = "module_zone".spaceship_model_id
JOIN "spaceship".module_slot ON "module_zone".module_zone_id = "module_slot".module_zone_id
--!find_by_member! WHERE "spaceship".member_id = $1
--!find_by_id! WHERE "spaceship_model".spaceship_model_id = $1
GROUP BY
	"spaceship_model".spaceship_model_id