use crate::{spaceship_model::models::NewSpaceshipModel, auth::models::Claims, member::services::find_one_member_by_id, member::services::update_member_credits, spaceship::{services::{find_spaceship_by_member_and_spaceship_model, insert_spaceship, find_spaceship_by_model, delete_spaceships_by_model}, models::NewSpaceship}, shield::services::remove_spaceship_id_from_shield_by_spaceship, engine::services::remove_spaceship_id_from_engine_by_spaceship, cargo::services::remove_spaceship_id_from_cargo_by_spaceship, weapon::services::remove_spaceship_id_from_weapon_by_spaceship};
use actix_web::{Result, HttpResponse, web::{Data, Json, block, Path, ReqData}, Error, http::header::ContentType};
use serde_json::json;
use uuid::Uuid;
use crate::DBPool;
use crate::DBPooledConnection;

use super::{services::{insert_spaceship_model, find_all_spaceship_models, find_one_spaceship_model_by_name, find_one_spaceship_model_by_id, find_spaceship_models_by_member, insert_module_zone, insert_module_slot, find_module_zone_by_model, delete_module_slot_by_zone, delete_module_zone_by_model, delete_spaceship_model}, models::{InsertSpaceshipModel, NewModuleZone, NewModuleSlot}};

pub async fn create(spaceship_model: Json<InsertSpaceshipModel>, db: Data<DBPool>) -> Result<HttpResponse, Error> {
	let spaceship_model_name: String = spaceship_model.name.clone();

	let name_conn = db.get().unwrap();
	let is_spaceship_model_same_name_exists = block(move || find_one_spaceship_model_by_name(spaceship_model_name, &name_conn))
		.await
		.unwrap();

	if is_spaceship_model_same_name_exists.is_ok() {
		return Ok(HttpResponse::Conflict().json(json!({ "message": "Spaceship model with same name already exists" })));
	}

	let new_spaceship_model = NewSpaceshipModel {
		name: spaceship_model.name.clone(),
		max_health: spaceship_model.max_health,
		spaceship_type: spaceship_model.spaceship_type.clone(),
		tank_capacity: spaceship_model.tank_capacity,
		max_power: spaceship_model.max_power,
		consumption_for_power: spaceship_model.consumption_for_power,
		energy_repartition_delay: spaceship_model.energy_repartition_delay,
		price: spaceship_model.price,
	};

	let spaceship_model_conn = db.get().unwrap();
	let spaceship_model_insertion = block(move || insert_spaceship_model(new_spaceship_model, &spaceship_model_conn))
		.await
		.unwrap();

	let spaceship_model_inserted = match spaceship_model_insertion {
		Ok(spaceship_model) => spaceship_model,
		Err(_) => return Ok(HttpResponse::InternalServerError().json(json!({ "message": "Failed to insert spaceship model" }))),
	};

	for module_zone in spaceship_model.module_zones.iter() {
		let new_module_zone = NewModuleZone {
			spaceship_model_id: spaceship_model_inserted.spaceship_model_id,
			shielding: module_zone.shielding,
		};

		let module_zone_conn = db.get().unwrap();
		let module_zone_insertion = block(move || insert_module_zone(new_module_zone, &module_zone_conn))
			.await
			.unwrap();

		let module_zone_inserted = match module_zone_insertion {
			Ok(module_zone) => module_zone,
			Err(_) => return Ok(HttpResponse::InternalServerError().json(json!({ "message": "Failed to insert module zone" }))),
		};

		for module_slot in module_zone.module_slots.iter() {
			let new_module_slot = NewModuleSlot {
				module_zone_id: module_zone_inserted.module_zone_id,
				module_type: module_slot.module_type.clone(),
				size: module_slot.size,
			};

			let module_slot_conn = db.get().unwrap();
			let module_slot_inserted = block(move || insert_module_slot(new_module_slot, &module_slot_conn))
				.await
				.unwrap();

			if module_slot_inserted.is_err() {
				return Ok(HttpResponse::InternalServerError().json(json!({ "message": "Failed to insert module slot" })));
			}
		}
	}

	return Ok(HttpResponse::Created().finish());
}

pub async fn delete(spaceship_model_id: Path<Uuid>, db: Data<DBPool>) -> Result<HttpResponse, Error> {
	let spaceship_model_id = spaceship_model_id.into_inner();

	let conn_ships = db.get().unwrap();
	let spaceships_found = block(move || find_spaceship_by_model(&spaceship_model_id.clone(), &conn_ships))
		.await
		.unwrap();

	let spaceships = match spaceships_found {
		Ok(spaceships) => spaceships,
		Err(_) => return Ok(HttpResponse::InternalServerError().json(json!({ "message": "Error while retrieving spaceships" }))),
	}; 

	for spaceship in spaceships.iter() {
		let spaceship_id = spaceship.spaceship_id.clone();

		let conn_shield = db.get().unwrap();
		let _ = block(move || remove_spaceship_id_from_shield_by_spaceship(&spaceship_id.clone(), &conn_shield))
			.await
			.unwrap();

		let conn_engine = db.get().unwrap();
		let _ = block(move || remove_spaceship_id_from_engine_by_spaceship(&spaceship_id.clone(), &conn_engine))
			.await
			.unwrap();

		let conn_weapon = db.get().unwrap();
		let _ = block(move || remove_spaceship_id_from_weapon_by_spaceship(&spaceship_id.clone(), &conn_weapon))
			.await
			.unwrap();

		let conn_cargo = db.get().unwrap();
		let _ = block(move || remove_spaceship_id_from_cargo_by_spaceship(&spaceship_id.clone(), &conn_cargo))
			.await
			.unwrap();
	}

	let conn_delete_ships = db.get().unwrap();
	let _ = block(move || delete_spaceships_by_model(&spaceship_model_id.clone(), &conn_delete_ships))
		.await
		.unwrap();

	let conn_module_zone = db.get().unwrap();
	let module_zones_found = block(move || find_module_zone_by_model(&spaceship_model_id.clone(), &conn_module_zone))
		.await
		.unwrap();

	let module_zones = match module_zones_found {
		Ok(module_zones) => module_zones,
		Err(_) => return Ok(HttpResponse::InternalServerError().json(json!({ "message": "Error while retrieving module zones" }))),
	};

	for module_zone in module_zones.iter() {
		let module_zone_id = module_zone.module_zone_id.clone();

		let conn_delete_slot = db.get().unwrap();
		let _ = block(move || delete_module_slot_by_zone(&module_zone_id, &conn_delete_slot))
			.await
			.unwrap();
	}

	let conn_delete_module_zone = db.get().unwrap();
	let _ = block(move || delete_module_zone_by_model(&spaceship_model_id.clone(), &conn_delete_module_zone))
		.await
		.unwrap();

	let conn_delete_spaceship_model = db.get().unwrap();
	let _ = block(move || delete_spaceship_model(&spaceship_model_id.clone(), &conn_delete_spaceship_model))
		.await
		.unwrap();

	return Ok(HttpResponse::NoContent().finish());
}

pub async fn retrieve_spaceship_models_by_member(claims: ReqData<Claims>, db: Data<DBPool>) -> Result<HttpResponse, Error> {
	let conn = db.get().unwrap();

	let member_id: Uuid = Uuid::parse_str(&claims.sub.clone()).unwrap();

	let spaceship_models_found = block(move || find_spaceship_models_by_member(&member_id, &conn))
	.await
	.unwrap();

	match spaceship_models_found {
		Ok(spaceship_models_found) => Ok(HttpResponse::Ok().json(spaceship_models_found)),
		Err(err) => {
			return Ok(HttpResponse::InternalServerError().json(json!({
				"message": format!("Error while retrieving spaceship model owned by member: {}", err.to_string()),
			})));
		},
	}
}

pub async fn list(db: Data<DBPool>) -> Result<HttpResponse, Error> {
	let conn: DBPooledConnection = db.get().unwrap();

	let spaceship_models = block(move || find_all_spaceship_models(&conn))
		.await
		.unwrap();

	match spaceship_models {
		Ok(spaceship_models) => {
			return Ok(HttpResponse::Ok().content_type(ContentType::json()).json(spaceship_models));
		},
		Err(_) => {
			return Ok(HttpResponse::InternalServerError().json(json!({ "message": "Failed to get spaceship models" })));
		}
	}
}

pub async fn buy(claims: ReqData<Claims>, spaceship_model_id: Path<Uuid>, db: Data<DBPool>) -> Result<HttpResponse, Error> {
	let conn: DBPooledConnection = db.get().unwrap();
	let is_spaceship_model_exists = block(move || find_one_spaceship_model_by_id(&spaceship_model_id.into_inner(), &conn))
		.await
		.unwrap();

	let spaceship_model = match is_spaceship_model_exists {
		Ok(spaceship_model) => spaceship_model,
		Err(_) => return Ok(HttpResponse::NotFound().json(json!({ "message": "Spaceship model not found" }))),
	};

	let member_id = Uuid::parse_str(&claims.sub.clone()).unwrap();
	let sec_conn: DBPooledConnection = db.get().unwrap();
	let is_member_exists = block(move || find_one_member_by_id(member_id, &sec_conn))
		.await
		.unwrap();

	let member = match is_member_exists {
		Ok(member) => member,
		Err(_) => return Ok(HttpResponse::NotFound().json(json!({ "message": "Member not found" }))),
	};

	if member.credits < spaceship_model.price {
		return Ok(HttpResponse::BadRequest().json(json!({ "message": "Not enough credits" })));
	}

	let fourth_conn: DBPooledConnection = db.get().unwrap();
	let is_spaceship_model_already_owned = block(move || find_spaceship_by_member_and_spaceship_model(
		&member.member_id,
		&spaceship_model.spaceship_model_id,
		&fourth_conn
	))
		.await
		.unwrap();

	if is_spaceship_model_already_owned.is_ok() {
		return Ok(HttpResponse::Conflict().json(json!({ "message": "Spaceship model already owned" })));
	}

	let new_spaceship = NewSpaceship {
		member_id: member.member_id,
		spaceship_model_id: spaceship_model.spaceship_model_id,
		health: spaceship_model.max_health,
		is_main_ship: false,
	};

	let fifth_conn: DBPooledConnection = db.get().unwrap();
	let insert_new_spaceship = block(move || insert_spaceship(
		new_spaceship,
		&fifth_conn
	))
		.await
		.unwrap();

	if insert_new_spaceship.is_err() {
		return Ok(HttpResponse::InternalServerError().json(json!({ "message": "Failed to insert new spaceship" })));
	}

	let payment: i32 = member.credits - spaceship_model.price;
	let third_conn: DBPooledConnection = db.get().unwrap();
	let _ = block(move || update_member_credits(member_id, payment, &third_conn))
		.await
		.unwrap();

	return Ok(HttpResponse::Ok().json(json!({ "message": "Successfully bought spaceship model" })));
}