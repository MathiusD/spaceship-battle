use crate::configuration::models::{LightModuleSlots, LightModuleZones, ModuleType};
use crate::schema::spaceship::spaceship_model;
use crate::schema::spaceship::module_slot;
use crate::schema::spaceship::module_zone;
use diesel_derive_enum::DbEnum;
use diesel::sql_types::{Integer, Text, Uuid, Jsonb};
use serde::{Serialize, Deserialize};
use diesel::{Insertable, Queryable};

#[derive(Debug, Serialize, Clone, DbEnum, Deserialize)]
#[DbValueStyle = "SCREAMING_SNAKE_CASE"]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum SpaceshipType {
	Basic,
	Fighter,
}

// Retourné par le back. Utilisé pour renvoyer un spaceship model complet (modules compris)
#[derive(Debug, Queryable, QueryableByName, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct SpaceshipModel {
	#[sql_type = "Uuid"]
	pub spaceship_model_id: uuid::Uuid,
	#[sql_type = "Text"]
	pub name: String,
	#[sql_type = "Integer"]
	pub max_health: i32,
	#[sql_type = "SpaceshipTypeMapping"]
	pub spaceship_type: SpaceshipType,
	#[sql_type = "Integer"]
	pub tank_capacity: i32,
	#[sql_type = "Integer"]
	pub max_power: i32,
	#[sql_type = "Integer"]
	pub consumption_for_power: i32,
	#[sql_type = "Integer"]
	pub energy_repartition_delay: i32,
	#[sql_type = "Integer"]
	pub price: i32,
	#[sql_type = "Jsonb"]
	pub module_zones: LightModuleZones,
	#[sql_type = "Jsonb"]
	pub module_slots: LightModuleSlots,
}

// Retourné par le back. Utilisé pour retourner un spaceship model seul
#[derive(Debug, Deserialize, Serialize, Queryable)]
#[serde(rename_all = "camelCase")]
pub struct LightSpaceshipModel {
	pub spaceship_model_id: uuid::Uuid,
	pub name: String,
	pub max_health: i32,
	pub spaceship_type: SpaceshipType,
	pub tank_capacity: i32,
	pub max_power: i32,
	pub consumption_for_power: i32,
	pub energy_repartition_delay: i32,
	pub price: i32,
}

// Utilisé dans le back. Utilisé pour insérer un nouveau spaceship model seul
#[derive(Insertable, Debug, Deserialize, Serialize, AsChangeset)]
#[table_name = "spaceship_model"]
#[serde(rename_all(deserialize = "camelCase"))]
pub struct NewSpaceshipModel {
	pub name: String,
	pub max_health: i32,
	pub spaceship_type: SpaceshipType,
	pub tank_capacity: i32,
	pub max_power: i32,
	pub consumption_for_power: i32,
	pub energy_repartition_delay: i32,
	pub price: i32,
}

// Envoyé par le front. Utilisé pour insérer un spaceship model complet (modules compris)
#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all(deserialize = "camelCase"))]
pub struct InsertSpaceshipModel {
	pub name: String,
	pub max_health: i32,
	pub spaceship_type: SpaceshipType,
	pub tank_capacity: i32,
	pub max_power: i32,
	pub consumption_for_power: i32,
	pub energy_repartition_delay: i32,
	pub price: i32,
	pub module_zones: Vec<InsertModuleZone>,
}

// Envoyé par le front. Utilisé pour insérer un module zone
#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all(deserialize = "camelCase"))]
pub struct InsertModuleZone {
	pub shielding: i32,
	pub module_slots: Vec<InsertModuleSlot>,
}

// Envoyé par le front. Utilisé pour insérer un module slot
#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all(deserialize = "camelCase"))]
pub struct InsertModuleSlot {
	pub module_type: ModuleType,
	pub size: i32,
}

// Utilisé par le back
#[derive(Debug, Deserialize, Serialize, Insertable, AsChangeset)]
#[table_name = "module_zone"]
#[serde(rename_all(deserialize = "camelCase"))]
pub struct NewModuleZone {
	pub spaceship_model_id: uuid::Uuid,
	pub shielding: i32,
}

// Utilisé par le back
#[derive(Debug, Deserialize, Serialize, Insertable, AsChangeset)]
#[table_name = "module_slot"]
#[serde(rename_all(deserialize = "camelCase"))]
pub struct NewModuleSlot {
	pub module_zone_id: uuid::Uuid,
	pub module_type: ModuleType,
	pub size: i32,
}

// Renvoyé par le back
#[derive(Debug, Deserialize, Serialize, Queryable)]
#[serde(rename_all(deserialize = "camelCase"))]
pub struct ModuleZone {
	pub module_zone_id: uuid::Uuid,
	pub spaceship_model_id: uuid::Uuid,
	pub shielding: i32,
}

// Renvoyé par le back
#[derive(Debug, Deserialize, Serialize, Queryable)]
#[serde(rename_all(deserialize = "camelCase"))]
pub struct ModuleSlot {
	pub module_slot_id: uuid::Uuid,
	pub module_zone_id: uuid::Uuid,
	pub module_type: ModuleType,
	pub size: i32,
}