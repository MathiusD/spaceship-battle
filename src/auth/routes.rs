use crate::auth::models::{Login, Register, Claims};
use crate::constants::SALT;
use crate::constants::SECRET_KEY;
use crate::member::services::{find_one_member_by_name, find_one_member_by_email, insert_member};
use actix_web::web::{block, ReqData};
use actix_web::Error;
use chrono::DateTime;
use serde_json::json;

use crate::DBPool;
use actix_web::{
	web::{Data, Json},
	HttpResponse,
};
use argon2::Config;
use chrono::{Duration, Utc};

use super::models::TokenType;
use super::services::token_encoding;

pub async fn login(member_login: Json<Login>, db: Data<DBPool>) -> Result<HttpResponse, Error> {
	let conn = db.get().unwrap();

	let member_login_email: String = member_login.email.clone();

	let member_found = block(move || find_one_member_by_email(member_login_email, &conn))
		.await
		.unwrap();

	match member_found {
		Ok(member_found) => {
			// 340ms pour vérifier le mot de passe
			// Il faudra trouver plus efficace
			let verify_password: bool = argon2::verify_encoded_ext(
				&member_found.password,
				member_login.password.as_bytes(),
				SECRET_KEY.as_bytes(),
				&[],
			)
			.unwrap();

			if verify_password {
				let expiration: DateTime<Utc> = Utc::now() + Duration::days(1);

				let claims = Claims {
					sub: member_found.member_id.to_string(),
					exp: expiration.timestamp(),
					is_admin: member_found.is_admin,
					access_type: TokenType::AccessToken,
				};

				let token = token_encoding(claims);

				match token {
					Ok(token) => {
						return Ok(HttpResponse::Ok().json(json!({
							"message": "Login successful",
							"token": token,
							"expiration": expiration.to_rfc3339(),
						})));
					},
					Err(_) => {
						return Ok(HttpResponse::InternalServerError().finish());
					}
				}
			} else {
				return Ok(HttpResponse::Unauthorized().json(json!({
					"message": "Invalid password",
				})));
			}
		},
		Err(_) => Ok(HttpResponse::Unauthorized().json(json!({
			"message": "Member not found",
		}))),
	}
}

pub async fn register(member_register: Json<Register>, db: Data<DBPool>) -> HttpResponse {
	// Vérifie si c'est un email
	if !member_register.email.contains("@") {
		return HttpResponse::BadRequest().json(json!({
			"message": "Invalid email",
		}));
	}

	// Vérifie si le mot de passe a la bonne longueur
	if member_register.password.len() < 8 || member_register.password.len() > 32 {
		return HttpResponse::BadRequest().json(json!({
			"message": "Password must be between 8 and 32 characters",
		}));
	}

	// Vérifie si le pseudo a la bonne longueur
	if member_register.name.len() < 4 || member_register.name.len() > 16 {
		return HttpResponse::BadRequest().json(json!({
			"message": "Name must be between 3 and 16 characters",
		}));
	}

	let conn_email = db.get().unwrap();
	let register_email = member_register.email.clone();
	let is_email_already_used = block(move || find_one_member_by_email(register_email, &conn_email))
		.await
		.unwrap();

	if is_email_already_used.is_ok() {
		return HttpResponse::Conflict().json(json!({
			"message": "Email already used",
		}));
	}

	let conn_name = db.get().unwrap();
	let register_name = member_register.name.clone();
	let is_name_already_used = block(move || find_one_member_by_name(register_name, &conn_name))
		.await
		.unwrap();

	if is_name_already_used.is_ok() {
		return HttpResponse::Conflict().json(json!({
			"message": "Name already used",
		}));
	}

	let is_ascii = member_register.password.chars().all(|c| c.is_ascii());

	if !is_ascii {
		return HttpResponse::BadRequest().json(json!({
			"message": "Password should not contains illegal characters",
		}));
	}

	let is_there_lowercase = member_register.password.chars().any(|c| c.is_ascii_lowercase());
	let is_there_uppercase = member_register.password.chars().any(|c| c.is_ascii_uppercase());
	let is_there_digit = member_register.password.chars().any(|c| c.is_ascii_digit());
	let is_there_special_chars = member_register.password.chars().any(|c| c.is_ascii_punctuation());

	if !is_there_lowercase || !is_there_uppercase || !is_there_digit || !is_there_special_chars {
		return HttpResponse::BadRequest().json(json!({
			"message": "Password must contain at least one lowercase, one uppercase, one digit and one special character"
		}));
	}

	let is_there_whitespaces = member_register.password.chars().any(|c| c.is_ascii_whitespace());

	if is_there_whitespaces {
		return HttpResponse::BadRequest().json(json!({
			"message": "Password must not contain whitespaces"
		}));
	}

	let config: Config = Config {
		secret: SECRET_KEY.as_bytes(),
		..Default::default()
	};

	let hash_processing = argon2::hash_encoded(member_register.password.clone().as_bytes(), &SALT, &config);

	let hash: String = match hash_processing {
		Ok(hash) => hash,
		Err(_) => {
			return HttpResponse::InternalServerError().json(json!({
				"message": "Error while hashing password"
			}));
		}
	};

	let register = Register {
		name: member_register.name.clone(),
		password: hash,
		email: member_register.email.clone(),
	};

	let conn_insert = db.get().unwrap();
	let new_member = block(move || insert_member(register, &conn_insert))
		.await
		.unwrap();

	match new_member {
		Ok(inserted_member) => HttpResponse::Created().json(inserted_member),
		Err(err) => HttpResponse::InternalServerError().json(json!({
			"message": format!("Error while creating member: {:?}", err),
		})),
	}
}

pub async fn websocket_login(claims: ReqData<Claims>) -> Result<HttpResponse, Error> {
	let expiration: DateTime<Utc> = Utc::now() + Duration::seconds(5);

	let claims = Claims {
		sub: claims.sub.clone(),
		exp: expiration.timestamp(),
		is_admin: claims.is_admin,
		access_type: TokenType::WebSocketToken,
	};

	let token = token_encoding(claims);
	match token {
		Ok(token) => Ok(HttpResponse::Ok().json(json!({
			"message": "Login successful",
			"token": token,
			"expiration": expiration.to_rfc3339(),
		}))),
		Err(_) => Ok(HttpResponse::InternalServerError().finish()),
	}
}