use actix_web::{
	web::{block, Data, Json, Path, ReqData},
	Error, HttpResponse,
};
use serde_json::json;
use uuid::Uuid;

use crate::{
	auth::models::Claims,
	cargo::services::{remove_spaceship_id_from_cargo, update_by_cargo_id_and_member},
	engine::services::{
		remove_spaceship_id_from_engine, update_by_engine_id_and_member,
	},
	shield::services::{remove_spaceship_id_from_shield, update_by_shield_id_and_member},
	spaceship::services::{
		find_spaceship_by_id_and_member_id, remove_name_from_spaceship, update_main_configuration, update_name_by_spaceship_and_member,
	},
	spaceship_model::{models::SpaceshipModel, services::find_one_spaceship_model_by_id},
	weapon::services::{remove_spaceship_id_from_weapon, update_by_weapon_id_and_member},
	DBPool, engine_model::{services::find_one_engine_model_by_engine_id, models::EngineModel}, cargo_model::{services::find_one_cargo_model_by_cargo_id, models::CargoModel}, shield_model::{services::find_one_shield_model_by_shield_id, models::ShieldModel}, weapon_model::{services::find_one_weapon_model_by_weapon_id, models::WeaponModel},
};

use super::{
	models::{ConfigurationLight, ModuleType},
	services::{
		find_configurations_by_member, find_main_configuration_by_member,
		find_one_configuration_by_member,
	},
};

pub async fn retrieve_main_configuration_by_member(
	claims: ReqData<Claims>,
	db: Data<DBPool>,
) -> Result<HttpResponse, Error> {
	let conn = db.get().unwrap();

	let member_id: Uuid = Uuid::parse_str(&claims.sub.clone()).unwrap();

	let main_configuration_found =
		block(move || find_main_configuration_by_member(&member_id, &conn))
			.await
			.unwrap();

	match main_configuration_found {
		Ok(configuration) => Ok(HttpResponse::Ok().json(configuration)),
		Err(err) => {
			return Ok(HttpResponse::NotFound().json(json!({
				"message": format!("Error while retrieving main configuration: {}", err.to_string()),
			})));
		}
	}
}

pub async fn retrieve_configurations_by_member(
	claims: ReqData<Claims>,
	db: Data<DBPool>,
) -> Result<HttpResponse, Error> {
	let conn = db.get().unwrap();

	let member_id: Uuid = Uuid::parse_str(&claims.sub.clone()).unwrap();

	let configurations_found = block(move || find_configurations_by_member(&member_id, &conn))
		.await
		.unwrap();

	match configurations_found {
		Ok(configurations) => Ok(HttpResponse::Ok().json(configurations)),
		Err(err) => {
			return Ok(HttpResponse::NotFound().json(json!({
				"message": format!("Error while retrieving configurations: {}", err.to_string()),
			})));
		}
	}
}

pub async fn delete_configuration_by_member(
	claims: ReqData<Claims>,
	db: Data<DBPool>,
	requested_spaceship_id: Path<Uuid>,
) -> Result<HttpResponse, Error> {
	let member_id: Uuid = Uuid::parse_str(&claims.sub.clone()).unwrap();
	let spaceship_id: Uuid = requested_spaceship_id.into_inner();

	let conn = db.get().unwrap();
	let configuration_found =
		block(move || find_one_configuration_by_member(&member_id, &spaceship_id, &conn))
			.await
			.unwrap();

	let configuration = match configuration_found {
		Ok(config) => match config {
			Some(config) => config,
			None => {
				return Ok(HttpResponse::NotFound().json(json!({
					"message": format!("No configuration found for this member and spaceship")
				})));
			}
		},
		Err(err) => {
			return Ok(HttpResponse::InternalServerError().json(json!({
				"message": format!("Error while retrieving configuration: {}", err.to_string())
			})))
		}
	};

	let conn_spaceship = db.get().unwrap();
	let remove_name_from_spaceship = block(move || {
		remove_name_from_spaceship(
			&configuration.spaceship_id.clone(),
			&member_id.clone(),
			&conn_spaceship,
		)
	})
	.await
	.unwrap();

	if remove_name_from_spaceship.is_err() {
		return Ok(HttpResponse::InternalServerError().json(json!({
			"message":
				format!(
					"Error while removing name from spaceship: {}",
					remove_name_from_spaceship.err().unwrap().to_string()
				)
		})));
	}

	let conn_main_ship = db.get().unwrap();
	if configuration.is_main_ship {
		let update_result =
            block(move || update_main_configuration(&spaceship_id, false, &conn_main_ship))
                .await
                .unwrap();

        if update_result.is_err() {
            return Ok(HttpResponse::InternalServerError().json(
                json!({"message": "Error while updating old main configurations to false"}),
            ));
        }
	}

	let engine_id = configuration.engine.engine_id.clone();

	let conn_engine = db.get().unwrap();
	let remove_engine_from_spaceship = block(move || {
		remove_spaceship_id_from_engine(&engine_id, &member_id.clone(), &conn_engine)
	})
	.await
	.unwrap();

	if remove_engine_from_spaceship.is_err() {
		return Ok(HttpResponse::InternalServerError()
			.json(json!({"message": "Error while removing engine from spaceship"})));
	}

	match configuration.shield {
		Some(shield) => {
			let shield_id = shield.shield_id.clone();

			let conn_shield = db.get().unwrap();
			let remove_shield_from_spaceship = block(move || {
				remove_spaceship_id_from_shield(&shield_id, &member_id.clone(), &conn_shield)
			})
			.await
			.unwrap();

			if remove_shield_from_spaceship.is_err() {
				return Ok(HttpResponse::InternalServerError()
					.json(json!({"message": "Error while removing shield from spaceship"})));
			}
		}
		None => (),
	};

	match configuration.cargo {
		Some(cargo) => {
			let cargo_id = cargo.cargo_id.clone();

			let conn_cargo = db.get().unwrap();
			let remove_cargo_from_spaceship = block(move || {
				remove_spaceship_id_from_cargo(&cargo_id, &member_id.clone(), &conn_cargo)
			})
			.await
			.unwrap();

			if remove_cargo_from_spaceship.is_err() {
				return Ok(HttpResponse::InternalServerError()
					.json(json!({"message": "Error while removing cargo from spaceship"})));
			}
		}
		None => (),
	};

	let weapon_ids: Vec<Uuid> = configuration
		.weapons
		.list
		.iter()
		.map(|w| w.weapon_id.clone())
		.collect();

	for weapon_id in weapon_ids {
		let conn_weapon = db.get().unwrap();

		let remove_weapon_from_spaceship = block(move || {
			remove_spaceship_id_from_weapon(&weapon_id, &member_id.clone(), &conn_weapon)
		})
		.await
		.unwrap();

		if remove_weapon_from_spaceship.is_err() {
			return Ok(HttpResponse::InternalServerError()
				.json(json!({"message": "Error while removing weapon from spaceship"})));
		}
	}

	return Ok(
		HttpResponse::NoContent().json(json!({"message": "Deleted configuration successfully"}))
	);
}

pub async fn create_configuration_by_member(
	claims: ReqData<Claims>,
	db: Data<DBPool>,
	new_configuration: Json<ConfigurationLight>,
) -> Result<HttpResponse, Error> {
	let member_id: Uuid = Uuid::parse_str(&claims.sub.clone()).unwrap();

	let spaceship_id = new_configuration.spaceship_id.clone();

	let conn_spaceship = db.get().unwrap();
	let spaceship_found = block(move || {
		find_spaceship_by_id_and_member_id(&spaceship_id, &member_id, &conn_spaceship)
	})
	.await
	.unwrap();

	let spaceship = match spaceship_found {
		Ok(spaceship) => match spaceship {
			Some(spaceship) => spaceship,
			None => {
				return Ok(HttpResponse::NotFound().json(json!({
					"message": format!("No spaceship found for this member and spaceship id")
				})));
			}
		},
		Err(err) => {
			return Ok(HttpResponse::InternalServerError().json(json!({
				"message": format!("Error while retrieving spaceship: {}", err.to_string())
			})))
		}
	};

	let conn_spaceship_model = db.get().unwrap();
	let spaceship_model_found = block(move || {
		find_one_spaceship_model_by_id(&spaceship.spaceship_model_id.clone(), &conn_spaceship_model)
	})
	.await
	.unwrap();

	let spaceship_model: SpaceshipModel = match spaceship_model_found {
		Ok(spaceship_model) => spaceship_model,
		Err(err) => {
			return Ok(HttpResponse::InternalServerError().json(json!({
				"message":
					format!(
						"Error while retrieving spaceship model: {}",
						err.to_string()
					)
			})))
		}
	};

	// Check module in correct slot and size

	let engine_id = new_configuration.engine.engine_id.clone();
	let engine_module_slot_id = new_configuration.engine.module_slot_id.clone();
	
	let conn_engine_model = db.get().unwrap();
	let engine_model_found = block(move || find_one_engine_model_by_engine_id(&engine_id, &conn_engine_model))
		.await
		.unwrap();

	let engine_model: EngineModel = match engine_model_found {
		Ok(engine_model) => engine_model,
		Err(err) => {
			return Ok(HttpResponse::InternalServerError().json(json!({
				"message": format!("Error while retrieving engine model: {}",err.to_string())
			})));
		}
	};

	let is_slot_correct_for_engine: bool = spaceship_model.module_slots.list.iter().any(|s| {
		s.module_type == ModuleType::Engine
			&& s.module_slot_id == new_configuration.engine.module_slot_id
				&& s.size >= engine_model.size
	});

	if !is_slot_correct_for_engine {
		return Ok(HttpResponse::BadRequest()
			.json(json!({"message": "The module slot is not compatible with the engine"})));
	}

	match &new_configuration.cargo.cargo_id {
		Some(cargo_id) => {
			let cargo_id = cargo_id.clone();

			let conn_cargo_model = db.get().unwrap();
			let cargo_model_found = block(move || {
				find_one_cargo_model_by_cargo_id(&cargo_id.clone(), &conn_cargo_model)
			})
			.await
			.unwrap();

			let cargo_model: CargoModel = match cargo_model_found {
				Ok(cargo_model) => cargo_model,
				Err(err) => {
					return Ok(HttpResponse::InternalServerError().json(json!({
						"message": format!("Error while retrieving cargo model: {}",err.to_string())
					})));
				}
			};

			let is_slot_correct_for_cargo = spaceship_model.module_slots.list.iter().any(|s| {
				s.module_type == ModuleType::Cargo
					&& s.module_slot_id == new_configuration.cargo.module_slot_id
						&& s.size >= cargo_model.size
			});

			if !is_slot_correct_for_cargo {
				return Ok(HttpResponse::BadRequest()
					.json(json!({"message": "The module slot is not compatible with the cargo"})));
			}
		}
		None => (),
	}

	match &new_configuration.shield.shield_id {
		Some(shield_id) => {
			let shield_id = shield_id.clone();
			
			let conn_shield_model = db.get().unwrap();
			let shield_model_found = block(move || {
				find_one_shield_model_by_shield_id(&shield_id.clone(), &conn_shield_model)
			})
			.await
			.unwrap();

			let shield_model: ShieldModel = match shield_model_found {
				Ok(shield_model) => shield_model,
				Err(err) => {
					return Ok(HttpResponse::InternalServerError().json(json!({
						"message": format!("Error while retrieving shield model: {}",err.to_string())
					})));
				}
			};

			let is_slot_correct_for_shield = spaceship_model.module_slots.list.iter().any(|s| {
				s.module_type == ModuleType::Shield
					&& s.module_slot_id == new_configuration.shield.module_slot_id
						&& s.size >= shield_model.size
			});

			if !is_slot_correct_for_shield {
				return Ok(HttpResponse::BadRequest().json(
					json!({"message": "The module slot is not compatible with the shield"}),
				));
			}
		}
		None => (),
	}

	for weapon in new_configuration.weapons.iter() {
		if let Some(weapon_id) = weapon.weapon_id {
			let conn_weapon_model = db.get().unwrap();
			let weapon_model_found = block(move || {
				find_one_weapon_model_by_weapon_id(&weapon_id.clone(), &conn_weapon_model)
			})
			.await
			.unwrap();

			let weapon_model: WeaponModel = match weapon_model_found {
				Ok(weapon_model) => weapon_model,
				Err(err) => {
					return Ok(HttpResponse::InternalServerError().json(json!({
						"message": format!("Error while retrieving weapon model: {}",err.to_string())
					})));
				}
			};

			let is_slot_correct_for_weapon = spaceship_model.module_slots.list.iter().any(|s| {
				s.module_type == ModuleType::Weapon
					&& s.module_slot_id == weapon.module_slot_id
						&& s.size >= weapon_model.size
			});
	
			if !is_slot_correct_for_weapon {
				return Ok(HttpResponse::BadRequest()
					.json(json!({"message": "The module slot is not compatible with the weapon"})));
			}
		}
	}

	// Update ship

	let conn_main_spaceship = db.get().unwrap();
	let main_configuration_found = block(move || find_main_configuration_by_member(
		&spaceship_id,
		&conn_main_spaceship,
	))
	.await
	.unwrap();

	let is_main_configuration: bool = match main_configuration_found {
		Ok(main_configuration) => if let Some(_) = main_configuration { false } else { true },
		Err(err) => {
			return Ok(HttpResponse::InternalServerError().json(json!({
				"message": format!("Error while retrieving main configuration: {}",err.to_string())
			})));
		}
	};

	let name = new_configuration.name.clone();

	let conn_update_spaceship = db.get().unwrap();
	let update_spaceship = block(move || {
		update_name_by_spaceship_and_member(
			&spaceship_id,
			&member_id,
			&name,
			&is_main_configuration,
			&conn_update_spaceship,
		)
	})
	.await
	.unwrap();

	if update_spaceship.is_err() {
		return Ok(HttpResponse::InternalServerError()
			.json(json!({"message": "Error while updating spaceship name"})));
	}

	let conn_engine = db.get().unwrap();
	let update_engine = block(move || {
		update_by_engine_id_and_member(
			&engine_id,
			&engine_module_slot_id,
			&member_id.clone(),
			&spaceship_id.clone(),
			&conn_engine,
		)
	})
	.await
	.unwrap();

	if update_engine.is_err() {
		return Ok(HttpResponse::InternalServerError().json(json!({
			"message": "Error while updating engine"
		})));
	}

	match &new_configuration.shield.shield_id {
		Some(shield_id) => {
			let shield_id = shield_id.clone();
			let shield_module_slot_id = new_configuration.shield.module_slot_id.clone();


			let conn_shield = db.get().unwrap();
			let update_shield = block(move || {
				update_by_shield_id_and_member(
					&shield_id,
					&shield_module_slot_id,
					&member_id,
					&spaceship_id.clone(),
					&conn_shield,
				)
			})
			.await
			.unwrap();

			if update_shield.is_err() {
				return Ok(HttpResponse::InternalServerError().json(json!({
					"message": "Error while updating shield"
				})));
			}
		}
		None => (),
	};

	match &new_configuration.cargo.cargo_id {
		Some(cargo_id) => {
			let cargo_id = cargo_id.clone();
			let cargo_module_slot_id = new_configuration.cargo.module_slot_id.clone();

			let conn_cargo = db.get().unwrap();
			let update_cargo = block(move || {
				update_by_cargo_id_and_member(
					&cargo_id,
					&cargo_module_slot_id,
					&member_id,
					&spaceship_id.clone(),
					&conn_cargo,
				)
			})
			.await
			.unwrap();

			if update_cargo.is_err() {
				return Ok(HttpResponse::InternalServerError().json(json!({
					"message": "Error while updating shield"
				})));
			}
		}
		None => (),
	};

	for weapon in new_configuration.weapons.iter() {
		let conn_weapon = db.get().unwrap();
		let weapon_id = match weapon.weapon_id {
			Some(ref weapon_id) => weapon_id.clone(),
			None => continue,
		};
		let weapon_module_slot_id = weapon.module_slot_id.clone();

		let update_weapon = block(move || {
			update_by_weapon_id_and_member(
				&weapon_id,
				&weapon_module_slot_id,
				&member_id,
				&spaceship_id.clone(),
				&conn_weapon,
			)
		})
		.await
		.unwrap();

		if update_weapon.is_err() {
			return Ok(HttpResponse::InternalServerError().json(json!({
				"message": "Error while updating weapon"
			})));
		}
	}

	return Ok(HttpResponse::Created().json(json!({"message": "Configuration created"})));
}
