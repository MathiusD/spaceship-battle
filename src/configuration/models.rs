use diesel::{types::FromSql, pg::Pg, sql_types::{Integer, Jsonb, Text, Nullable, Uuid, Bool}};
use serde::{Serialize, Deserialize};
use crate::spaceship_model::models::{SpaceshipType, SpaceshipTypeMapping};
use crate::cargo::models::CargoComponent;
use crate::{
	engine::models::EngineComponent, shield::models::ShieldComponent, weapon::models::WeaponComponent,
};
use diesel_derive_enum::DbEnum;

#[derive(Debug, Clone, Serialize, DbEnum, Deserialize, PartialEq)]
#[DbValueStyle = "SCREAMING_SNAKE_CASE"]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum ModuleType {
	Cargo,
	Shield,
	Weapon,
	Engine,
	Computer,
	Sensor,
	Mining,
}

#[derive(QueryableByName, Clone, Debug, Queryable, Serialize)]
#[serde(rename_all(serialize = "camelCase"))]
pub struct Configuration {
	#[sql_type = "Uuid"]
	pub spaceship_id: uuid::Uuid,
	#[sql_type = "Text"]
	pub name: String,
	#[sql_type = "Uuid"]
	pub member_id: uuid::Uuid,
	#[sql_type = "Integer"]
	pub health: i32,
	#[sql_type = "Integer"]
	pub max_health: i32,
	#[sql_type = "Uuid"]
	pub spaceship_model_id: uuid::Uuid,
	#[sql_type = "Text"]
	pub model_name: String,
	#[sql_type = "SpaceshipTypeMapping"]
	pub spaceship_type: SpaceshipType,
	#[sql_type = "Integer"]
	pub tank_capacity: i32,
	#[sql_type = "Integer"]
	pub max_power: i32,
	#[sql_type = "Integer"]
	pub consumption_for_power: i32,
	#[sql_type = "Integer"]
	pub energy_repartition_delay: i32,
	#[sql_type = "Bool"]
	pub is_main_ship: bool,
	#[sql_type = "Jsonb"]
	pub module_zones: LightModuleZones,
	#[sql_type = "Jsonb"]
	pub module_slots: LightModuleSlots,
	#[sql_type = "Jsonb"]
	pub engine: EngineComponent,
	#[sql_type = "Jsonb"]
	pub weapons: Weapons,
	#[sql_type = "Nullable<Jsonb>"]
	pub shield: Option<ShieldComponent>,
	#[sql_type = "Nullable<Jsonb>"]
	pub cargo: Option<CargoComponent>,
}

#[derive(AsExpression, Clone, Debug, Serialize, Deserialize)]
#[sql_type = "Jsonb"]
#[serde(transparent)]
pub struct OptionCargoComponent {
	pub cargo_component: Option<CargoComponent>,
}


#[derive(AsExpression, Clone, Debug, Serialize, Deserialize)]
#[sql_type = "Jsonb"]
#[serde(transparent)]
pub struct LightModuleZones {
	pub list: Vec<LightModuleZone>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all(serialize = "camelCase"))]
pub struct LightModuleZone {
	pub shielding: i32,
	pub module_zone_id: uuid::Uuid,
}

#[derive(AsExpression, Debug, Clone, Serialize, Deserialize)]
#[sql_type = "Jsonb"]
#[serde(transparent)]
pub struct LightModuleSlots {
	pub list: Vec<LightModuleSlot>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all(serialize = "camelCase"))]
pub struct LightModuleSlot {
	pub module_slot_id: uuid::Uuid,
	pub module_zone_id: uuid::Uuid,
	pub module_type: ModuleType,
	pub size: i32,
}

#[derive(AsExpression, Clone, Debug, Serialize, Deserialize)]
#[sql_type = "Jsonb"]
#[serde(transparent)]
pub struct Weapons {
	pub list: Vec<WeaponComponent>,
}

impl FromSql<Jsonb, Pg> for ShieldComponent {
	fn from_sql(bytes: Option<&[u8]>) -> diesel::deserialize::Result<Self> {
		let value = <serde_json::Value as FromSql<Jsonb, Pg>>::from_sql(bytes)?;
		Ok(serde_json::from_value(value)?)
	}
}

impl FromSql<Nullable<Jsonb>, Pg> for ShieldComponent {
	fn from_sql(bytes: Option<&[u8]>) -> diesel::deserialize::Result<Self> {
		let value = <serde_json::Value as FromSql<Jsonb, Pg>>::from_sql(bytes)?;
		Ok(serde_json::from_value(value)?)
	}
}

impl FromSql<Jsonb, Pg> for LightModuleZones {
	fn from_sql(bytes: Option<&[u8]>) -> diesel::deserialize::Result<Self> {
		let value = <serde_json::Value as FromSql<Jsonb, Pg>>::from_sql(bytes)?;
		Ok(serde_json::from_value(value)?)
	}
}

impl FromSql<Jsonb, Pg> for Weapons {
	fn from_sql(bytes: Option<&[u8]>) -> diesel::deserialize::Result<Self> {
		let value = <serde_json::Value as FromSql<Jsonb, Pg>>::from_sql(bytes)?;
		Ok(serde_json::from_value(value)?)
	}
}

impl FromSql<Jsonb, Pg> for EngineComponent {
	fn from_sql(bytes: Option<&[u8]>) -> diesel::deserialize::Result<Self> {
		let value = <serde_json::Value as FromSql<Jsonb, Pg>>::from_sql(bytes)?;
		Ok(serde_json::from_value(value)?)
	}
}

impl FromSql<Jsonb, Pg> for LightModuleSlots {
	fn from_sql(bytes: Option<&[u8]>) -> diesel::deserialize::Result<Self> {
		let value = <serde_json::Value as FromSql<Jsonb, Pg>>::from_sql(bytes)?;
		Ok(serde_json::from_value(value)?)
	}
}

impl FromSql<Jsonb, Pg> for CargoComponent {
	fn from_sql(bytes: Option<&[u8]>) -> diesel::deserialize::Result<Self> {
		let value = <serde_json::Value as FromSql<Jsonb, Pg>>::from_sql(bytes)?;
		Ok(serde_json::from_value(value)?)
	}
}

impl FromSql<Nullable<Jsonb>, Pg> for CargoComponent {
	fn from_sql(bytes: Option<&[u8]>) -> diesel::deserialize::Result<Self> {
		let value = <serde_json::Value as FromSql<Jsonb, Pg>>::from_sql(bytes)?;
		Ok(serde_json::from_value(value)?)
	}
}

#[derive(Debug, Deserialize, Serialize)]
pub struct UpdateMainConfiguration {
	spaceship_id: uuid::Uuid,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
#[serde(rename_all(deserialize = "camelCase"))]
pub struct ConfigurationLight {
	pub name: String,
	pub spaceship_id: uuid::Uuid,
	pub engine: EngineLightConfiguration,
	pub cargo: CargoLightConfiguration,
	pub shield: ShieldLightConfiguration,
	pub weapons: Vec<WeaponLightConfiguration>,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
#[serde(rename_all(deserialize = "camelCase"))]
pub struct CargoLightConfiguration {
	pub cargo_id: Option<uuid::Uuid>,
	pub module_slot_id: uuid::Uuid,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
#[serde(rename_all(deserialize = "camelCase"))]
pub struct EngineLightConfiguration {
	pub engine_id: uuid::Uuid,
	pub module_slot_id: uuid::Uuid,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
#[serde(rename_all(deserialize = "camelCase"))]
pub struct ShieldLightConfiguration {
	pub shield_id: Option<uuid::Uuid>,
	pub module_slot_id: uuid::Uuid,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
#[serde(rename_all(deserialize = "camelCase"))]
pub struct WeaponLightConfiguration {
	pub weapon_id: Option<uuid::Uuid>,
	pub module_slot_id: uuid::Uuid,
}