use serde::{Serialize, Deserialize};
use uuid::Uuid;
use crate::cargo_model::models::CargoType;
use crate::schema::spaceship::cargo;

#[derive(Debug, Clone, Queryable, Serialize, Deserialize)]
#[serde(rename_all(serialize = "camelCase"))]
pub struct Cargo {
    pub cargo_id: Uuid,
    pub spaceship_id: Option<Uuid>,
    pub cargo_model_id: Uuid,
    pub member_id: Uuid,
    pub module_slot_id: Option<Uuid>,
    pub health: i32,
    pub created_at: Option<chrono::NaiveDateTime>,
    pub updated_at: Option<chrono::NaiveDateTime>,
}

#[derive(Insertable, Debug, Deserialize, Serialize)]
#[table_name = "cargo"]
pub struct NewCargo {
	pub cargo_model_id: Uuid,
	pub health: i32,
	pub member_id: Uuid,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all(serialize = "camelCase"))]
pub struct CargoComponent {
	pub cargo_id: Uuid,
	pub cargo_model_id: Uuid,
	pub module_slot_id: Uuid,
	pub health: i32,
	pub max_health: i32,
	pub name: String,
	pub cargo_type: CargoType,
	pub capacity: i32,

}