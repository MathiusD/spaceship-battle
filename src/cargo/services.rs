use crate::diesel::ExpressionMethods;
use diesel::{result::Error, BoolExpressionMethods, OptionalExtension, QueryDsl, RunQueryDsl};
use uuid::Uuid;

use crate::{schema::spaceship::cargo, DBPooledConnection};

use super::models::{Cargo, NewCargo};

pub fn find_cargo_list_by_member(
    member_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<Vec<Cargo>, Error> {
    return cargo::table
        .filter(cargo::member_id.eq(member_id))
        .load::<Cargo>(conn);
}

pub fn insert_cargo(new_cargo: NewCargo, conn: &DBPooledConnection) -> Result<Cargo, Error> {
    return diesel::insert_into(cargo::table)
        .values(&new_cargo)
        .get_result(conn);
}

pub fn find_cargo_by_member_and_cargo_model(
    member_id: &Uuid,
    cargo_model_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<Cargo, Error> {
    return cargo::table
        .filter(
            cargo::member_id
                .eq(member_id)
                .and(cargo::cargo_model_id.eq(cargo_model_id)),
        )
        .first::<Cargo>(conn);
}

pub fn find_one_cargo_by_cargo_id_and_member(
    cargo_id: &Uuid,
    member_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<Option<Cargo>, Error> {
    return cargo::table
        .filter(
            cargo::cargo_id
                .eq(cargo_id)
                .and(cargo::member_id.eq(member_id)),
        )
        .first::<Cargo>(conn)
        .optional();
}

pub fn remove_spaceship_id_from_cargo(
    cargo_id: &Uuid,
    member_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<usize, Error> {
    let result = diesel::update(
        cargo::table.filter(
            cargo::cargo_id
                .eq(cargo_id)
                .and(cargo::member_id.eq(member_id))
                .and(cargo::spaceship_id.is_not_null())
                .and(cargo::module_slot_id.is_not_null()),
        ),
    )
    .set((
        cargo::spaceship_id.eq(None::<Uuid>),
        cargo::module_slot_id.eq(None::<Uuid>),
    ))
    .execute(conn);
    if Ok(0) == result {
        return Err(Error::NotFound);
    }
    return result;
}

pub fn update_by_cargo_id_and_member(
    cargo_id: &Uuid,
    module_slot_id: &Uuid,
    member_id: &Uuid,
    spaceship_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<usize, Error> {
    let result = diesel::update(
        cargo::table.filter(
            cargo::cargo_id
                .eq(cargo_id)
                .and(cargo::member_id.eq(member_id))
                .and(
                    cargo::spaceship_id
                        .is_null()
                        .or(cargo::spaceship_id.eq(spaceship_id)),
                ),
        ),
    )
    .set((
        cargo::spaceship_id.eq(spaceship_id),
        cargo::module_slot_id.eq(module_slot_id),
    ))
    .execute(conn);
    if Ok(0) == result {
        return Err(Error::NotFound);
    }
    return result;
}

pub fn update_spaceship_id_from_cargo(
    cargo_id: &Uuid,
    member_id: &Uuid,
    spaceship_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<usize, Error> {
    let result = diesel::update(
        cargo::table.filter(
            cargo::cargo_id
                .eq(cargo_id)
                .and(cargo::member_id.eq(member_id))
                .and(cargo::spaceship_id.is_null()),
        ),
    )
    .set(cargo::spaceship_id.eq(spaceship_id))
    .execute(conn);
    if Ok(0) == result {
        return Err(Error::NotFound);
    }
    return result;
}

pub fn remove_all_cargos_by_member(
    member_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<usize, Error> {
    return diesel::delete(cargo::table.filter(cargo::member_id.eq(member_id))).execute(conn);
}

pub fn delete_cargos_by_model(
    cargo_model_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<usize, Error> {
    return diesel::delete(cargo::table.filter(cargo::cargo_model_id.eq(cargo_model_id)))
        .execute(conn);
}

pub fn remove_spaceship_id_from_cargo_by_spaceship(
    spaceship_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<usize, Error> {
    return diesel::update(cargo::table.filter(cargo::spaceship_id.eq(spaceship_id)))
        .set(cargo::spaceship_id.eq(None::<Uuid>))
        .execute(conn);
}
