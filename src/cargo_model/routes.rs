use crate::{cargo_model::models::NewCargoModel, auth::models::Claims, member::services::{find_one_member_by_id, update_member_credits}, cargo::{services::{find_cargo_by_member_and_cargo_model, insert_cargo, delete_cargos_by_model}, models::NewCargo}};
use actix_web::{Result, HttpResponse, web::{Data, Json, block, ReqData, Path}, Error, http::header::ContentType};
use serde_json::json;
use crate::DBPool;
use uuid::Uuid;
use crate::DBPooledConnection;

use super::services::{insert_cargo_model, find_one_cargo_model_by_name, find_all_cargo_models, find_one_cargo_model_by_id, find_cargo_models_by_member, update_cargo_model, delete_cargo_model};

pub async fn create(cargo_model: Json<NewCargoModel>, db: Data<DBPool>) -> Result<HttpResponse, Error> {
	let conn: DBPooledConnection = db.get().unwrap();

	let cargo_model_name: String = cargo_model.name.clone();

	let is_cargo_model_same_name_exists = block(move || find_one_cargo_model_by_name(cargo_model_name, &conn))
.await
.unwrap();

	match is_cargo_model_same_name_exists {
		Ok(_) => {
			return Ok(HttpResponse::Conflict().json(json!({
				"message": "Cargo model with same name already exists",
			})));
		},
		Err(_) => {
			let cargo_model_inserted = block(move || insert_cargo_model(cargo_model.into_inner(), &db.get().unwrap()))
.await
.unwrap();

			match cargo_model_inserted {
				Ok(cargo_model_inserted) => {
					return Ok(HttpResponse::Created().content_type(ContentType::json()).json(cargo_model_inserted));
				},
				Err(err) => {
					return Ok(HttpResponse::InternalServerError().json(json!({
						"message": format!("Failed to insert cargo model: {}", err.to_string()),
					})));
				}
			}
		}
	}
}

pub async fn list(db: Data<DBPool>) -> Result<HttpResponse, Error> {
	let conn: DBPooledConnection = db.get().unwrap();

	let cargo_models = block(move || find_all_cargo_models(&conn))
.await
.unwrap();

	match cargo_models {
		Ok(cargo_models) => {
			return Ok(HttpResponse::Ok().content_type(ContentType::json()).json(cargo_models));
		},
		Err(_) => {
			return Ok(HttpResponse::InternalServerError().json(json!({ "message": "Failed to get cargo models" })));
		}
	}
}

pub async fn buy(claims: ReqData<Claims>, cargo_model_id: Path<Uuid>, db: Data<DBPool>) -> Result<HttpResponse, Error> {
	let conn: DBPooledConnection = db.get().unwrap();
	let is_cargo_model_exists = block(move || find_one_cargo_model_by_id(&cargo_model_id.into_inner(), &conn))
		.await
		.unwrap();

	let cargo_model = match is_cargo_model_exists {
		Ok(cargo_model) => cargo_model,
		Err(_) => return Ok(HttpResponse::NotFound().json(json!({ "message": "Cargo model not found" }))),
	};

	let sec_conn: DBPooledConnection = db.get().unwrap();
	let member_id = Uuid::parse_str(&claims.sub.clone()).unwrap();
	let is_member_exists = block(move || find_one_member_by_id(member_id, &sec_conn))
		.await
		.unwrap();

	let member = match is_member_exists {
		Ok(member) => member,
		Err(_) => return Ok(HttpResponse::NotFound().json(json!({ "message": "Member not found" }))),
	};

	if member.credits < cargo_model.price {
		return Ok(HttpResponse::BadRequest().json(json!({ "message": "Not enough credits" })));
	}

	let fourth_conn: DBPooledConnection = db.get().unwrap();
	let is_cargo_model_already_owned = block(move || find_cargo_by_member_and_cargo_model(
		&member.member_id,
		&cargo_model.cargo_model_id,
		&fourth_conn
	))
		.await
		.unwrap();

	if is_cargo_model_already_owned.is_ok() {
		return Ok(HttpResponse::Conflict().json(json!({ "message": "Cargo model already owned" })));
	}

	let new_cargo = NewCargo {
		member_id: member.member_id,
		cargo_model_id: cargo_model.cargo_model_id,
		health: cargo_model.max_health,
	};

	let fifth_conn: DBPooledConnection = db.get().unwrap();
	let insert_new_cargo = block(move || insert_cargo(
		new_cargo,
		&fifth_conn
	))
		.await
		.unwrap();

	if insert_new_cargo.is_err() {
		return Ok(HttpResponse::InternalServerError().json(json!({ "message": "Failed to insert new cargo" })));
	}

	let payment: i32 = member.credits - cargo_model.price;
	let third_conn: DBPooledConnection = db.get().unwrap();
	let _ = block(move || update_member_credits(member_id, payment, &third_conn))
		.await
		.unwrap();

	return Ok(HttpResponse::Ok().json(json!({ "message": "Successfully bought cargo model" })));
}

pub async fn retrieve_cargo_models_by_member(claims: ReqData<Claims>, db: Data<DBPool>) -> Result<HttpResponse, Error> {
	let conn = db.get().unwrap();

	let member_id: Uuid = Uuid::parse_str(&claims.sub.clone()).unwrap();

	let cargo_models_found = block(move || find_cargo_models_by_member(&member_id, &conn))
		.await
		.unwrap();

	match cargo_models_found {
		Ok(cargo_models_found) => Ok(HttpResponse::Ok().json(cargo_models_found)),
		Err(err) => {
			return Ok(HttpResponse::InternalServerError().json(json!({
				"message": format!("Error while retrieving cargo model owned by member: {}", err.to_string()),
			})));
		},
	}
}

pub async fn update(cargo_model_id: Path<Uuid>, cargo_model: Json<NewCargoModel>, db: Data<DBPool>) -> Result<HttpResponse, Error> {
	let conn = db.get().unwrap();

	let cargo_model_updated = block(move || update_cargo_model(cargo_model_id.into_inner(), cargo_model.into_inner(), &conn))
		.await
		.unwrap();

	match cargo_model_updated {
		Ok(cargo_model_updated) => {
			return Ok(HttpResponse::Ok().json(json!(cargo_model_updated)));
		},
		Err(err) => {
			return Ok(HttpResponse::InternalServerError().json(json!({ "message": format!("Failed to update cargo model: {}", err.to_string())})));
		}
	}
}

pub async fn delete(cargo_model_id: Path<Uuid>, db: Data<DBPool>) -> Result<HttpResponse, Error> {
	let cargo_model_id = cargo_model_id.into_inner();

	let cargo_conn = db.get().unwrap();
	let _ = block(move || delete_cargos_by_model(&cargo_model_id.clone(), &cargo_conn))
		.await
		.unwrap();

	let cargo_model_conn = db.get().unwrap();
	let cargo_model_deleted = block(move || delete_cargo_model(&cargo_model_id.clone(), &cargo_model_conn))
		.await
		.unwrap();

	match cargo_model_deleted {
		Ok(_) => {
			return Ok(HttpResponse::NoContent().finish());
		},
		Err(err) => {
			return Ok(HttpResponse::InternalServerError().json(json!({ "message": format!("Failed to delete cargo model: {}", err.to_string())})));
		}
	}
}