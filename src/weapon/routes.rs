use actix_web::{HttpResponse, web::{block, ReqData, Data}, Error};
use serde_json::json;
use uuid::Uuid;

use crate::{DBPool, auth::models::Claims};

use super::services::find_weapon_list_by_member;

pub async fn retrieve_weapons_by_member(claims: ReqData<Claims>, db: Data<DBPool>) -> Result<HttpResponse, Error> {
	let conn = db.get().unwrap();

	let member_id: Uuid = Uuid::parse_str(&claims.sub.clone()).unwrap();

	let weapons_found = block(move || find_weapon_list_by_member(&member_id, &conn))
		.await
		.unwrap();

	match weapons_found {
		Ok(weapons) => Ok(HttpResponse::Ok().json(weapons)),
		Err(err) => {
			return Ok(HttpResponse::NotFound().json(json!({
				"message": format!("No weapon owned found: {}", err.to_string()),
			})));
		},
	}	
}