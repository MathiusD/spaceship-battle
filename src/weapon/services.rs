use crate::diesel::BoolExpressionMethods;
use crate::diesel::ExpressionMethods;
use diesel::{OptionalExtension, QueryDsl, RunQueryDsl};
use uuid::Uuid;

use crate::{schema::spaceship::weapon, DBPooledConnection};

use super::models::{NewWeapon, Weapon};

pub fn find_weapon_list_by_member(
    member_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<Vec<Weapon>, diesel::result::Error> {
    return weapon::table
        .filter(weapon::member_id.eq(member_id))
        .load::<Weapon>(conn);
}

pub fn insert_weapon(
    new_weapon: NewWeapon,
    conn: &DBPooledConnection,
) -> Result<Weapon, diesel::result::Error> {
    return diesel::insert_into(weapon::table)
        .values(&new_weapon)
        .get_result(conn);
}

pub fn update_by_weapon_id_and_member(
    weapon_id: &Uuid,
    module_slot_id: &Uuid,
    member_id: &Uuid,
    spaceship_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<usize, diesel::result::Error> {
    let result = diesel::update(
        weapon::table.filter(
            weapon::weapon_id
                .eq(weapon_id)
                .and(weapon::member_id.eq(member_id))
                .and(
                    weapon::spaceship_id
                        .is_null()
                        .or(weapon::spaceship_id.eq(spaceship_id)),
                ),
        ),
    )
    .set((
        weapon::spaceship_id.eq(spaceship_id),
        weapon::module_slot_id.eq(module_slot_id),
    ))
    .execute(conn);
    if Ok(0) == result {
        return Err(diesel::result::Error::NotFound);
    }
    return result;
}

pub fn remove_spaceship_id_from_weapon(
    weapon_id: &Uuid,
    member_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<usize, diesel::result::Error> {
    let result = diesel::update(
        weapon::table.filter(
            weapon::weapon_id
                .eq(weapon_id)
                .and(weapon::member_id.eq(member_id))
                .and(weapon::spaceship_id.is_not_null())
                .and(weapon::module_slot_id.is_not_null()),
        ),
    )
    .set((
        weapon::spaceship_id.eq(None::<Uuid>),
        weapon::module_slot_id.eq(None::<Uuid>),
    ))
    .execute(conn);
    if Ok(0) == result {
        return Err(diesel::result::Error::NotFound);
    }
    return result;
}

pub fn find_one_weapon_by_weapon_id_and_member(
    weapon_id: &Uuid,
    member_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<Option<Weapon>, diesel::result::Error> {
    return weapon::table
        .filter(
            weapon::weapon_id
                .eq(weapon_id)
                .and(weapon::member_id.eq(member_id)),
        )
        .first::<Weapon>(conn)
        .optional();
}

pub fn find_weapon_by_member_and_weapon_model(
    member_id: &Uuid,
    weapon_model_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<Weapon, diesel::result::Error> {
    return weapon::table
        .filter(
            weapon::member_id
                .eq(member_id)
                .and(weapon::weapon_model_id.eq(weapon_model_id)),
        )
        .first::<Weapon>(conn);
}

pub fn update_spaceship_id_from_weapon(
    weapon_id: &Uuid,
    member_id: &Uuid,
    spaceship_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<usize, diesel::result::Error> {
    let result = diesel::update(
        weapon::table.filter(
            weapon::weapon_id
                .eq(weapon_id)
                .and(weapon::member_id.eq(member_id))
                .and(weapon::spaceship_id.is_null()),
        ),
    )
    .set(weapon::spaceship_id.eq(spaceship_id))
    .execute(conn);
    if Ok(0) == result {
        return Err(diesel::result::Error::NotFound);
    }
    return result;
}

pub fn remove_all_weapons_by_member(
    member_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<usize, diesel::result::Error> {
    return diesel::delete(weapon::table.filter(weapon::member_id.eq(member_id))).execute(conn);
}

pub fn delete_weapons_by_model(
    weapon_model_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<usize, diesel::result::Error> {
    return diesel::delete(weapon::table.filter(weapon::weapon_model_id.eq(weapon_model_id)))
        .execute(conn);
}

pub fn remove_spaceship_id_from_weapon_by_spaceship(
    spaceship_id: &Uuid,
    conn: &DBPooledConnection,
) -> Result<usize, diesel::result::Error> {
    return diesel::update(weapon::table.filter(weapon::spaceship_id.eq(spaceship_id)))
        .set(weapon::spaceship_id.eq(None::<Uuid>))
        .execute(conn);
}
