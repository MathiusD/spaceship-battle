use serde::{Serialize, Deserialize};
use uuid::Uuid;

use crate::schema::spaceship::engine;

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all(serialize = "camelCase"))]
pub struct EngineComponent {
	pub engine_id: Uuid,
	pub module_slot_id: Uuid,
	pub name: String,
	pub health: i32,
	pub max_health: i32,
	pub max_power: i32,
	pub max_speed: i32,
	pub engine_model_id: Uuid,
}

#[derive(Debug, Clone, Queryable, Serialize, Deserialize)]
#[serde(rename_all(serialize = "camelCase"))]
pub struct Engine {
	pub engine_id: Uuid,
	pub spaceship_id: Option<Uuid>,
	pub engine_model_id: Uuid,
	pub member_id: Uuid,
	pub module_slot_id: Option<Uuid>,
	pub health: i32,
	pub created_at: Option<chrono::NaiveDateTime>,
	pub updated_at: Option<chrono::NaiveDateTime>,
}

#[derive(Insertable, Debug, Deserialize, Serialize)]
#[table_name = "engine"]
pub struct NewEngine {
	pub engine_model_id: Uuid,
	pub health: i32,
	pub member_id: Uuid,
}