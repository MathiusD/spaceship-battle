use actix_web::{HttpResponse, web::{block, ReqData, Data}, Error};
use serde_json::json;
use uuid::Uuid;

use crate::{DBPool, auth::models::Claims};

use super::services::find_engine_list_by_member;

pub async fn retrieve_engines_by_member(claims: ReqData<Claims>, db: Data<DBPool>) -> Result<HttpResponse, Error> {
	let conn = db.get().unwrap();

	let member_id: Uuid = Uuid::parse_str(&claims.sub.clone()).unwrap();

	let engines_found = block(move || find_engine_list_by_member(&member_id, &conn))
		.await
		.unwrap();

	match engines_found {
		Ok(engines) => Ok(HttpResponse::Ok().json(engines)),
		Err(err) => {
			return Ok(HttpResponse::NotFound().json(json!({
				"message": format!("No engine owned found: {}", err.to_string()),
			})));
		},
	}	
}