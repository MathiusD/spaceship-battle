use std::time::Instant;
use actix::{Addr, AsyncContext, ActorContext};
use actix_web_actors::ws::WebsocketContext;
use uuid::Uuid;

use crate::{constants::{HEARTBEAT_INTERVAL, CLIENT_TIMEOUT}, game::services::events::disconnection_event::Disconnect, configuration::models::Configuration};

use super::game_server::GameServer;

#[derive(Debug)]
pub struct Player {
	pub member_id: Uuid,
	pub name: String,
	pub spaceship: Configuration,
	pub game_server: Addr<GameServer>,
	pub room_id: Option<Uuid>,
	pub hb: Instant,
}

impl Player {
	pub fn hb(&self, ctx: &mut WebsocketContext<Self>) {
		ctx.run_interval(HEARTBEAT_INTERVAL, |act, ctx| {
			if Instant::now().duration_since(act.hb) > CLIENT_TIMEOUT {
				act.game_server.do_send(Disconnect {
					player_addr: ctx.address(),
					room_id: act.room_id.unwrap(),
				});
				ctx.stop();
				return;
			}
			ctx.ping(b"PING");
		});
	}
}
