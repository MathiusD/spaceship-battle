use super::room::Room;

#[derive(Debug)]
pub struct GameServer {
	pub rooms: Vec<Room>,
}

impl GameServer {
	pub fn new() -> GameServer {
		return GameServer { rooms: Vec::new() };
	}
}
