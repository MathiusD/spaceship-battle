use actix::Message;
use chrono::{DateTime, Utc};
use serde::Serialize;
use uuid::Uuid;

use crate::configuration::models::Configuration;

use super::room::RoomState;

#[derive(Debug, Serialize)]
pub enum BattleMessageType {
	JoinRoom,
	LeaveRoom,
	OpponentData,
	NewPlayerJoined,
	OpponentStatusUpdate,
	SpaceshipStatusUpdate,
	AttackProcessingResult,
}

#[derive(Debug, Serialize)]
#[serde(rename_all(serialize = "camelCase"))]
pub struct JoinRoomMessage {
	pub room_state: RoomState,
	pub room_id: Uuid,
	pub message_type: BattleMessageType,
}

impl JoinRoomMessage {
	pub fn new(room_state: RoomState, room_id: Uuid) -> Self {
		JoinRoomMessage {
			room_state,
			room_id,
			message_type: BattleMessageType::JoinRoom,
		}
	}
}

#[derive(Debug, Serialize)]
#[serde(rename_all(serialize = "camelCase"))]
pub struct NewPlayerJoinedRoomMessage {
	pub room_state: RoomState,
	pub player_name: String,
	pub player_spaceship: Configuration,
	pub message_type: BattleMessageType,
}

impl NewPlayerJoinedRoomMessage {
	pub fn new(room_state: RoomState, player_name: String, player_spaceship: Configuration) -> Self {
		NewPlayerJoinedRoomMessage {
			room_state,
			player_name,
			player_spaceship,
			message_type: BattleMessageType::NewPlayerJoined,
		}
	}
}

#[derive(Debug, Serialize, Message)]
#[serde(rename_all(serialize = "camelCase"))]
#[rtype(result = "()")]
pub struct LeaveRoomMessage {
	pub message_type: BattleMessageType,
	pub room_state: RoomState,
}

impl LeaveRoomMessage {
	pub fn new(room_state: RoomState) -> Self {
		LeaveRoomMessage {
			message_type: BattleMessageType::LeaveRoom,
			room_state,
		}
	}
}

#[derive(Debug, Serialize, Message)]
#[serde(rename_all(serialize = "camelCase"))]
#[rtype(result = "()")]
pub struct StatusUpdate {
	pub message_type: BattleMessageType,
	pub health: i32,
	pub has_won: bool,
	pub room_state: RoomState,
}

impl StatusUpdate {
	pub fn new(health: i32, message_type: BattleMessageType, has_won: bool, room_state: RoomState) -> Self {
		StatusUpdate {
			message_type,
			health,
			has_won,
			room_state
		}
	}
}

#[derive(Debug, Serialize)]
#[serde(rename_all(serialize = "camelCase"))]
pub struct AttackProcessingResult {
	pub message_type: BattleMessageType,
	pub ammos: i32,
	pub next_shot_time: DateTime<Utc>,
	pub weapon_id: Uuid,
}

impl AttackProcessingResult {
	pub fn new(ammos: i32, next_shot_time: DateTime<Utc>, weapon_id: Uuid) -> Self {
		AttackProcessingResult {
			message_type: BattleMessageType::AttackProcessingResult,
			ammos,
			next_shot_time,
			weapon_id,
		}
	}
}