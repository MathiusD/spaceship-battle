use actix::Addr;
use serde::Serialize;
use uuid::Uuid;

use super::player::Player;

#[derive(Debug, Serialize, Clone)]
pub enum RoomState {
	Refused,
	Wait,
	InGame,
	End,
}

#[derive(Debug)]
pub struct Room {
	pub room_id: Uuid,
	pub max_players: u8,
	pub players: Vec<Addr<Player>>,
	pub members_id: Vec<Uuid>,
	pub state: RoomState,
}

impl Room {
	pub fn new(player_id: Addr<Player>, member_id: Uuid) -> Room {
		return Room {
			room_id: Uuid::new_v4(),
			max_players: 2,
			players: vec![player_id],
			members_id: vec![member_id],
			state: RoomState::Wait,
		};
	}
}