use actix::{MessageResponse, Message, Addr, Handler, Context};
use uuid::Uuid;

use crate::{game::models::{player::Player, room::{RoomState, Room}, game_server::GameServer}, configuration::models::Configuration};

#[derive(Message)]
#[rtype(result = "ConnectionData")]
pub struct Connect {
	pub addr: Addr<Player>,
	pub player_name: String,
	pub member_id: Uuid,
	pub spaceship: Configuration,
}

#[derive(MessageResponse)]
pub struct ConnectionData {
	pub room_id: Uuid,
	pub room_state: RoomState,
	pub opponent_player_addr: Option<Addr<Player>>,
}

impl Handler<Connect> for GameServer {
	type Result = ConnectionData;

	fn handle(&mut self, msg: Connect, _ctx: &mut Context<Self>) -> Self::Result {
		let is_already_in_game = self.rooms.iter().any(|room| {
			room.members_id.contains(&msg.member_id)
		});

		if is_already_in_game {
			return ConnectionData {
				room_id: Uuid::parse_str("00000000-0000-0000-0000-000000000000").unwrap(),
				room_state: RoomState::Refused,
				opponent_player_addr: None,
			};
		}

		let is_free_room_found = self
			.rooms
			.iter()
			.position(|r| r.players.len() < 2 && matches!(r.state, RoomState::Wait));

		match is_free_room_found {
			Some(i) => {
				self.rooms[i].players.push(msg.addr);
				self.rooms[i].state = RoomState::InGame;
				return ConnectionData {
					room_id: self.rooms[i].room_id,
					room_state: RoomState::InGame,
					opponent_player_addr: Some(self.rooms[i].players[0].clone()),
				};
			}
			None => {
				let new_room = Room::new(msg.addr, msg.member_id);
				let room_id = new_room.room_id;
				self.rooms.push(new_room);
				return ConnectionData {
					room_id,
					room_state: RoomState::Wait,
					opponent_player_addr: None,
				};
			}
		};
	}
}