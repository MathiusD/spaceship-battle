use std::time::Duration;

use lazy_static::lazy_static;
use std::env;

pub const SALT: &'static [u8] = b"supersecuresalt";
pub const CLIENT_TIMEOUT: Duration = Duration::from_secs(10);
pub const HEARTBEAT_INTERVAL: Duration = Duration::from_secs(5);

lazy_static! {
    pub static ref SENTRY_KEY: Option<String> = match env::var("SENTRY_KEY") {
        Ok(key) => Option::Some(key),
        Err(_e) => Option::None,
    };
    pub static ref SECRET_KEY: String = env::var("SECRET_KEY").expect("Secret key not set");
    pub static ref HOST: String = env::var("HOST").expect("Host not set");
    pub static ref PORT: String = env::var("PORT").expect("Port not set");
    pub static ref DATABASE_URL: String = env::var("DATABASE_URL").expect("Database url not set");
}
