[[_TOC_]]

Jeu de duel de vaisseaux spatiaux en temps réel.

# Stack

## Serveur

- Actix
- Diesel
- PostgreSQL (Docker)

## Front

- Nuxt 3 (TypeScript)

# Schéma PostgreSQL

Attention, schéma du 29/08/22. Potentiellement pas à jour.

![alt text](spaceship.png "Diagramme de la base de données")

# Notes sur le dev

- Problème sur `schema.rs` : remplacer manuellement `xxx_type` par `XxxTypeMapping`.
- Penser à changer `Vec<Room>` en `HashMap`.
- Empêcher les attaques avant que la room soit en `RoomState::inGame`.
- À chaque attaque, attendre le resultat du message pour vérifier que la game est toujours en cours. Si c'est pas le cas, fermer l'acteur et terminer la partie.

# Notes de gameplay

## Composants du vaisseau

### Châssis

Le châssis dispose d'un blindage initial qui peut être différent pour chaque zone du vaisseau. Les dégâts infligés au vaisseau entament d'abord le blindage. Lorsque le blindage de toutes les zones du vaisseau est épuisé, les dégâts infligés au châssis attaquent directement la vie du vaisseau, ce qui provoque une baisse proportionnelle de la capacité du vaisseau à fournir de l'énergie aux composants.

Si la vie du vaisseau tombe à 0, ce dernier est détruit.

Le châssis contient le réservoir de carburant et le système énergétique. Ce dernier a un taux de conversion carburant/unité d'énergie et un taux de production d'énergie maximum par unité de temps.

Le châssis permet de régler la répartition de l'énergie entre les composants, pour consommer moins ou en délivrer un maximum à un composant en particulier. Il y a un délai, variable selon les châssis, entre l'envoi de l'ordre de changer la répartition et la réalisation de la modification. De plus, un délai est appliqué avant de pouvoir à nouveau changer la répartition.

### Boucliers

Le bouclier a une capacité d'absorption de dégâts. Les dégâts qui arrivent au vaisseau sont absorbés par le bouclier (à moins que des munitions spéciales soient utilisées). Lorsque la quantité de dégâts qu'il peut encaisser est dépassé, le bouclier est désactivé et les dégâts impactent directement le vaisseau. Le bouclier doit être désactivé pour qu'il se régénère. La vitesse de régénération dépend de la qualité du bouclier. Plus le bouclier a de points à régénérer, moins c'est rapide. La désactivation/réactivation du bouclier est énergivore.

### Zones du vaisseau

Le vaisseau est divisé en zones que l'adversaire peut attaquer. Chaque zone du vaisseau a un blindage qui lui est propre et héberge des composants du vaisseau (bouclier, moteur, armes...).

### Composants

Chaque composant du vaisseau peut encaisser un certain nombre de dégâts. Lorsque le nombre de dégâts dépassent la capacité du composant, le composant est désactivé. Le rôle tenu par le composant dans le vaisseau n'est plus assuré.

Chaque composant consomme de l'énergie lorsqu'il est utilisé. Ils ne fonctionneront à leur pleine puissance que si la quantité d'énergie idéale est disponible. Dans le cas contraire, une décôte est appliquée aux performances du composant (qui peut être logarithmique, exponentielle, etc... selon le composant).

## Choix du duel

On imagine que le jeu consiste globalement à voyager entre 2 planètes pour y transporter du cargo et le vendre, convoyer du cargo pour quelqu'un d'autre et faire payer ce service, voler du cargo ou récupérer une rançon. Donc 4 choix de type de duel.

- **Convoyeur :** Transporter une certaine quantité de cargo pour le compte d'un PNJ.
	- *Gameplay :* Choix entre affronter l'adversaire et le vaincre ou tenter de fuir.
	- *Avantages :** Possibilité de jouer sur la défensive, aucune perte de crédits en cas de défaite.
	- *Désavantages :* Gain de crédits limité, perte de réputation auprès des PNJ en cas de défaite.

- **Marchand :** Acheter sa propre marchandise et la transporter sur l'autre planète pour profiter des fluctuations de prix.
	- *Gameplay :* Choix entre affronter l'adversaire et le vaincre ou tenter de fuir.
	- *Avantages :* Possibilité de jouer sur la défensive, aucune perte de réputation en cas de défaite, gros gain de crédits possible.
	- *Désavantages :* Nécessité d'investir des crédits dans l'achat de marchandises, perte de sa marchandise en cas de défaite.

- **Intercepteur :** Voler le cargo des *convoyeurs* et des *marchands*.
	- *Gameplay :* Offensif.
	- *Avantages :* Aucun capital à investir dans l'achat de cargo, ni de mise en jeu de sa réputation auprès d'un marchand. Possibilité de récupérer dans son vaisseau le cargo de l'adversaire en cas de victoire, dans la limite des capacités de transport de son vaisseau.
	- *Désavantages :* Impossible de prédire si l'on vole un marchand ou un convoyeur. S'il s'agit d'un convoyeur, en cas de vol il y aura une perte de réputation. Risque d'avoir une prime sur sa tête. Risque de tomber sur un chasseur de primes.

- **Chasseur de primes :** Détruire des intercepteurs qui ont volé beaucoup de convoyeurs.
	- *Gameplay :* Offensif
	- *Avantages :* Prime importante, pas besoin de prévoir des emplacements de cargo dans son vaisseau. Gain de réputation en cas de victoire.
	- *Désavantages :* Perte de réputation en cas d'échec.
