CREATE TYPE weapon_type AS ENUM (
    'MISSILE',
    'GATLING',
    'RAILGUN'
);

CREATE TYPE damage_type AS ENUM (
    'KINETIC',
    'EXPLOSION',
    'ELECTRIC'
);

CREATE TYPE ammo_type AS ENUM (
    'HEAVY_AMMO',
    'ELECTRIC_BULK'
);

CREATE TABLE "spaceship".weapon_model (
    weapon_model_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    name TEXT UNIQUE NOT NULL, -- Nom du modèle d'arme
    weapon_type weapon_type NOT NULL, -- Type d'arme
    max_power INTEGER NOT NULL CHECK (max_power >= 0), -- Énergie demandée pour être au max des capacités de l'arme
    max_ammo INTEGER NOT NULL CHECK (max_ammo >= 0), -- Taille du chargeur
    max_health INTEGER NOT NULL CHECK (max_health >= 0), -- État initial de l'arme
    reload_time INTEGER NOT NULL, -- Temps de rechargement en ms
    damage INTEGER NOT NULL CHECK (damage >= 0), -- Dégâts infligés par l'arme
    damage_type damage_type NOT NULL, -- Type de dégâts
    ammo_type ammo_type NOT NULL, -- Type de munition
    travel_time_by_distance INTEGER NOT NULL, -- Temps de trajet en fonction de la distance avec la cible en ms/s
    min_shooting_distance INTEGER NOT NULL CHECK (min_shooting_distance >= 0), -- Distance minimal pour tirer
    max_shooting_distance INTEGER NOT NULL CHECK (max_shooting_distance >= min_shooting_distance), -- Distance maximale pour tirer
    size INTEGER NOT NULL, -- Taille de l'arme pour l'emplacement du module
    price INTEGER NOT NULL -- Coût du module
);
