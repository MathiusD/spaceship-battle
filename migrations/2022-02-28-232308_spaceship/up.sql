CREATE TABLE "spaceship".spaceship (
    spaceship_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    spaceship_model_id UUID REFERENCES "spaceship".spaceship_model (spaceship_model_id) NOT NULL,
    name TEXT, -- Nom du vaisseau
    member_id UUID REFERENCES "spaceship".member (member_id) NOT NULL,
    health INTEGER NOT NULL CHECK (health >= 0), -- État du vaisseau, équivaut à la somme de la vie des zones qui composent le vaisseau
    is_main_ship BOOLEAN NOT NULL DEFAULT FALSE, -- Vaisseau principal ?
    created_at TIMESTAMPTZ DEFAULT NOW(),
    updated_at TIMESTAMPTZ DEFAULT NOW()
);

CREATE TRIGGER spaceship_set_updated_at
    BEFORE UPDATE ON "spaceship".spaceship
    FOR EACH ROW
    EXECUTE PROCEDURE set_updated_at();