CREATE TYPE shield_type AS ENUM (
    'BASIC',
    'DISPERSION'
);

CREATE TABLE "spaceship".shield_model (
    shield_model_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    name TEXT UNIQUE NOT NULL, -- Nom du modèle de bouclier
    shield_type shield_type NOT NULL, -- Type de bouclier
    shield_type_protection INTEGER, -- Bonus de protection lié au type particulier du bouclier
    max_health INTEGER NOT NULL CHECK (max_health >= 0), -- Absorption des dégâts par le bouclier, ce qui est absorbé est déduit de la vie du bouclier
    reload_time INTEGER NOT NULL, -- Temps de rechargement du bouclier en ms
    max_power INTEGER NOT NULL, -- Énergie demandée pour être au max des capacités du bouclier
    size INTEGER NOT NULL, -- Taille du cargo pour l'emplacement du module
    price INTEGER NOT NULL -- Coût du module
);