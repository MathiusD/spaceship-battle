INSERT INTO "spaceship".weapon_model (
    weapon_model_id,
    name,
    weapon_type,
    max_power,
    max_ammo,
    max_health,
    reload_time,
    damage,
    damage_type,
    ammo_type,
    travel_time_by_distance,
    min_shooting_distance,
    max_shooting_distance,
    size,
    price
    ) VALUES
    ('e0e3c198-3826-422a-983d-9c251e3e36c7', 'ALEA ARMOR Freakish', 'MISSILE', 50, 8, 50, 2000, 40, 'EXPLOSION', 'HEAVY_AMMO', 0, 50, 200, 2, 2500),
    ('1d0f4fa1-751f-4c4f-93f5-cae0689db74e', 'IMRS Hyperion', 'RAILGUN', 75, 4, 50, 5000, 80, 'ELECTRIC', 'ELECTRIC_BULK', 0, 70, 160, 3, 4100),
    ('6067520e-7f1a-4413-a73b-5eaf22dda03f', 'ZEUGHAUS Verteidiger', 'GATLING', 20, 80, 50, 500, 10, 'KINETIC', 'HEAVY_AMMO', 0, 20, 100, 1, 1400);