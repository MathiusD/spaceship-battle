CREATE TYPE cargo_type AS ENUM (
    'BASIC',
    'RADIATION_PROTECTION'
);

CREATE TABLE "spaceship".cargo_model (
    cargo_model_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    name TEXT UNIQUE NOT NULL, -- Nom du modèle de cargo
    cargo_type cargo_type NOT NULL, -- Type de cargo
    max_health INTEGER NOT NULL CHECK (max_health >= 0),
    capacity INTEGER NOT NULL CHECK (capacity >= 0), -- Capacité du cargo
    size INTEGER NOT NULL CHECK (size >= 0), -- Taille du cargo pour l'emplacement du module
    price INTEGER NOT NULL -- Coût du module
);